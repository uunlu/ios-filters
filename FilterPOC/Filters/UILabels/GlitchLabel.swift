//
//  GlitchLabel.swift
//  FilterPOC
//
//  Created by Ugur Unlu on 4/30/20.
//  Copyright © 2020 Ugur Unlu. All rights reserved.
//

import UIKit

@IBDesignable
open class GlitchLabel: UILabel {
    @IBInspectable open var amplitudeBase: Double = 2.0
    @IBInspectable open var amplitudeRange: Double = 1.0
    
    @IBInspectable open var amplitude: Double = 8.0
    @IBInspectable open var threshold: Double = 0.8
    
    @IBInspectable open var alphaMin: Double = 0.8
    
    @IBInspectable open var drawScanLine: Bool = true
    
    open var blendMode: CGBlendMode = .multiply
    
    fileprivate var channel: Int = 0
    fileprivate var effectRatio: Double = 2.5
    fileprivate var phase: Double = 0.9
    fileprivate var phaseStep: Double = 0.05
    fileprivate var globalAlpha: Double = 0.5
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setTimer()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setTimer()
    }
    
    override open func drawText(in rect: CGRect) {
        
        var x0 = CGFloat(effectRatio * sin((.pi * 2.0) * phase))
        
        if random() >= threshold {
            x0 *= CGFloat(amplitude) + 5.0
        }
        
        let x1 = CGFloat(Int(bounds.origin.x))
        let x2 = x1 + x0
        let x3 = x1 - x0
        var channelImage: UIImage?
        
        globalAlpha = alphaMin + ((1 - alphaMin) * random())
        
        switch channel {
        case 0:
            channelImage = getChannelImage(x1, x2: x2, x3: x3)
        case 1:
            channelImage = getChannelImage(x2, x2: x3, x3: x1)
        case 2:
            channelImage = getChannelImage(x3, x2: x1, x3: x2)
        default:
            print("Error")
        }
        
        channelImage?.draw(in: bounds)
        
        if let channelsImage = channelImage, drawScanLine {
            getScanLineImage(channelsImage).draw(in: bounds)
            if floor(random() * 2) > 1 {
                getScanLineImage(channelsImage).draw(in: bounds)
            }
        }
    }
    
    fileprivate func getScanLineImage(_ channelImage: UIImage) -> UIImage {
        let y = bounds.size.height * CGFloat(random())
        let y2 = bounds.size.height * CGFloat(random())
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        
        let dataProvider = channelImage.cgImage!.dataProvider!
        let data = dataProvider.data! as NSData
        let bytes = data.bytes
        let bytePointer = bytes.assumingMemoryBound(to: UInt8.self)
        
        for col in 0 ..< Int(bounds.size.width) {
            let offset = 4*(Int(y) * Int(bounds.size.width) + col)
            let alpha = bytePointer[offset]
            let red = bytePointer[offset+1]
            let green = bytePointer[offset+2]
            let blue = bytePointer[offset+3]
            context?.setFillColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: CGFloat(alpha))
            context?.fill(CGRect(x: CGFloat(col), y: y2, width: 1, height: 0.5))
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    fileprivate func getChannelImage(_ x1: CGFloat, x2: CGFloat, x3:CGFloat) -> UIImage {
        let redImage = getImageFor(color: .red, rect: bounds)
        let greenImage = getImageFor(color: .green, rect: bounds)
        let blueImage = getImageFor(color: .blue, rect: bounds)
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        let redRect = CGRect(x: x1, y: 0, width: 0, height: 0)
        let greenRect = CGRect(x: x2, y: 0, width: 0, height: 0)
        let blueRect = CGRect(x: x3, y: 0, width: 0, height: 0)
        
        redImage.draw(in: bounds + redRect,
                      blendMode: blendMode,
                      alpha: CGFloat(globalAlpha))
        
        greenImage.draw(in: bounds + greenRect,
                        blendMode: blendMode,
                        alpha: CGFloat(globalAlpha))
        
        blueImage.draw(in: bounds + blueRect,
                       blendMode: blendMode,
                       alpha: CGFloat(globalAlpha))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    
    fileprivate func getImageFor(color: UIColor, rect: CGRect) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
        
        let nsText = NSString(string: text ?? "")
        nsText.draw(in: rect, withAttributes: [
            NSAttributedString.Key.font:UIFont.init(name: font.fontName, size: font.pointSize)!,
            NSAttributedString.Key.foregroundColor: color
        ])
        
        text = nsText as String
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    @objc fileprivate func tick() {
        
        phase += phaseStep
        if phase > 1 {
            phase = 0
            channel = (channel == 2) ? 0 : channel + 1
            effectRatio = amplitudeBase + (amplitudeRange * random())
        }
        
        setNeedsDisplay()
    }
    
    fileprivate func setTimer() {
        let timer = Timer(timeInterval: 1/30,
                          target: self, selector: #selector(GlitchLabel.tick),
                          userInfo: nil,
                          repeats: true)
        
        RunLoop.current.add(timer, forMode: .default)
    }
    
    fileprivate func random() -> Double {
        return (Double(arc4random())/Double(UINT32_MAX))
    }
}

func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}


func +(left: CGRect, right: CGRect) -> CGRect {
    
    return CGRect(x: left.origin.x + right.origin.x , y:left.origin.y + right.origin.y , width: left.size.width + right.size.width, height: left.size.height + right.size.height)
}

func +(left: CGSize, right: CGSize) -> CGSize {
    return CGSize(width: left.width + right.width, height: left.height + right.height)
}

